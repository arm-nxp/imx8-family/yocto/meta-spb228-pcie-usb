1. Including the spb228-pcie-usb layer:
   Add the line:
   BBLAYERS +=  " SRC_PATH/meta-spb228-pcie-usb "
   to your bblayers.conf file

2. Kernel configuration:
   mac80211 and wireless extensions support need to be included in the kernel configuration.
   The recipe meta-spb228-pcie-usb/recipes-kernel/linux/linux-imx_%.bbappend will do that automatically, but you will have to ensure that
   the filename (linux-imx_%.bbappend) matches your kernel name. The '%' is a wildcard that will match any character.

3. For debugging purposes, the showvars.bbclass in included in the meta-spb209a layer.
   It can be inherited anyware. Use it to run "bitbake -c showvars" <target> to print out all defined variables for the selected recipe.
