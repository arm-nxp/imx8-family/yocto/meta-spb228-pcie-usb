# Include kernel configuration fragment
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += "file://kconfig-fragment.cfg"
SRC_URI += "file://pcie-fragment.cfg"

do_configure_append() {
        #this is run from
        #./tmp/work/imx6dlsabresd-poky-linux-gnueabi/linux-imx/4.1.15-r0/git
        cat ../*.cfg >> ${B}/.config
}
